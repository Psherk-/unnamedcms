<!DOCTYPE html>
<html>
<head>
	<title>%name% ~ {title}</title>
	<link rel="stylesheet" type="text/css" href="http://%host%/public/%theme%/styles/index.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="http://%host%/public/%theme%/js/global.js"></script>
</head>
<body>
	<?php
		for($i = 0; $i < 10; i++)
			echo $i . '</br>';
	?>
	<div class="container">
		<div class="box left">
			<a href="http://%host%"><div class="logo"></div></a>
			<div class="container_login">
				<form method="post">
				<input type="text" class="input_blue" name="username" placeholder="{input.user}"/>
				<input type="password" class="input_orange" name="password" placeholder="{input.pass}"/>
				<input class="submit_login" type="submit" value="{input.login}"/>
				</form>
				<a href="#register"><button class="submit_register">{input.register}</button></a>
			</div>
		</div>
		<div class="box right">
			<div class="container_register">
				<div class="title">{input.reg.title}</div>
				<ul class="box reg">
					<form method="post">
					<li class="add"><input type="text" class="input_blue" name="reg_username" placeholder="{input.reg.user}"/></li>
					<li class="add"><input type="password" class="input_green" name="reg_pass" placeholder="{input.reg.pass}"/></li>
					<li class="add">{input.reg.birth}:</li>
					<li class="add">Captcha:</li>
					<li class="add"><input type="checkbox" name="reg_check"> {input.reg.terms}</li>
					<li class="add"><input class="submit_login" type="submit" value="{input.reg.enter}"/></li>
				</ul>
				<ul class="box reg">
					<li class="add"><input type="text" class="input_orange" name="reg_mail" placeholder="{input.reg.email}"/></li>
					<li class="add"><input type="password" class="input_yellow" name="reg_repass" placeholder="{input.reg.repass}"/></li>
					<li class="add"><input type="date" class="input_purple" name="reg_birth" placeholder="{input.reg.birth}" /></li>
					<li class="add"><div class="g-recaptcha" data-sitekey="%captcha_public_key%"></div></li>
					<li class="add"><input type="password" class="input_green" name="reg_pass" placeholder="{input.reg.pass}"/></form></li>
					<li class="add"><a href="#canregister"><button class="submit_register">{input.reg.cancel}</button></a></li>
				</ul>
			</div>
		</div>
	</div>
	<footer>
		
	</footer>
</body>
</html>