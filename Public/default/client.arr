<!DOCTYPE HTML>
<html>
<head>
    <title>%name% ~ {title}</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" href="http://localhost//habbo-web/america/pt/app.css">
    <link rel="stylesheet" type="text/css" href="http://localhost//game-data-server-static//./hotel.css">
    <script type="text/javascript" src="http://localhost//game-data-server-static//./habboapi.js"></script>
</head>
 
<script type="text/javascript">
    var flashvars = {
        "external.texts.txt": "%external_text%",
        "connection.info.port": "%client_port%",
        "furnidata.load.url": "%furnidata%",
        "external.variables.txt": "%external_vars%",
        "client.allow.cross.domain": "1",
        "url.prefix": "http://localhost",
        "external.override.texts.txt": "%external_text%",
        "supersonic_custom_css": "http://localhost/\/game-data-server-static\/\/.\/hotel.css",
        "external.figurepartlist.txt": "%figuredata%",
        "flash.client.origin": "popup",
        "client.starting": "Please wait! Habbo is starting up.",
        "processlog.enabled": "1",
        "has.identity": "1",
		"avatareditor.promohabbos":"https:\/\/www.habbo.es\/api\/public\/lists\/hotlooks",
        "productdata.load.url": "%productdata%",
        "client.starting.revolving": "For science, you monster\/Loading funny message\u2026please wait.\/Would you like fries with that?\/Follow the yellow duck.\/Time is just an illusion.\/Are we there yet?!\/I like your t-shirt.\/Look left. Look right. Blink twice. Ta da!\/It\'s not you, it\'s me.\/Shhh! I\'m trying to think here.\/Loading pixel universe.",
        "external.override.variables.txt": "%external_vars%",
        "spaweb": "1",
        "supersonic_application_key": "2c63c535",
        "connection.info.host": "%client_host%",
        "sso.ticket": "%ticket%",
        "client.notify.cross.domain": "0",
        "account_id": "1",
        "flash.client.url": "%gordon%",
        "unique_habbo_id": "hhes-3f744b55291fd2ebce42aea9fb9faae9",
    };
	</script>
	<script>
    var params = {
    "base": "%gordon%",
    "allowScriptAccess": "always",
    "menu": "false",
    "wmode": "opaque"
    };
    swfobject.embedSWF('%gordon%%swf%.swf', 'flash-container', '100%', '100%', '11.1.0', '//habboo-a.akamaihd.net/habboweb/63_1d5d8853040f30be0cc82355679bba7c/3630/web-gallery/flash/expressInstall.swf', flashvars, params, null, null);
    if (!(HabbletLoader.needsFlashKbWorkaround())) {
    params["wmode"] = "opaque";
    }
    FlashExternalInterface.signoutUrl = "http://localhost/client.php";
    </script>
<body id="client" class="flashclient">
 
 
<div id="overlay"></div>
<div id="client-ui" >
    <div id="flash-wrapper">
    <div id="flash-container">
    <div ng-if="isOpen &amp;&amp; !flashEnabled" class="client-error">
    <div class="client-error__text">
        <h1 class="client-error__title" translate="CLIENT_ERROR_TITLE">Ops, sem Flash, sem Lation!</h1>
        <p translate="CLIENT_ERROR_FLASH">Se você está utilizando um PC, você precisa <a href="http://www.adobe.com/go/getflashplayer" target="_blank">atualizar ou instalar o Flash player</a>.</p>
        <div class="client-error__downloads">
            <a href="http://www.adobe.com/go/getflashplayer" ng-href="http://www.adobe.com/go/getflashplayer" target="_blank" class="client-error__flash"></a>
        </div>
        <p translate="CLIENT_ERROR_MOBILE">Se você está utilizando um iPad, iPhone ou um dispositivo Android você deve baixar o <a href="https://itunes.apple.com/app/id794866182" target="_blank">Lation para iOS</a> na App Store ou <a href="https://play.google.com/store/apps/details?id=air.com.sulake.habboair" target="_blank">Lation para Android</a> na Google PlayStore.</p>
        <div class="client-error__downloads">
            <a href="https://itunes.apple.com/app/id794866182" ng-href="https://itunes.apple.com/app/id794866182" target="_blank" class="client-error__appstore"></a>
            <a href="https://play.google.com/store/apps/details?id=air.com.sulake.habboair" ng-href="https://play.google.com/store/apps/details?id=air.com.sulake.habboair" target="_blank" class="client-error__googleplay"></a>
      <script type="text/javascript">
    HabboView.run();
</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/
libs/jquery/1.3.0/jquery.min.js"></script>
  </div>
    </div>
</div>
    </div>
    </div>
</div>
 
</body>
</html>