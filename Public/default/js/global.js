$(document).ready(function() {
	function addConsole(event, text) {
		var TimeSync = new Date(),
			Time = TimeSync.getHours() + ":" + TimeSync.getMinutes() + ":" + TimeSync.getSeconds();
		if(event == null)
			console.log("["+Time+"] -> " + text);
		else
			console.log("["+Time+"]" + event + " -> " + text);
	}
	function getURL()
	{
		var loc = window.location,
		pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    	return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}

	/*MainCore*/
	addConsole(null, "UnnamedCMS <Psherk>"),
	addConsole(null, "Loaded in " + getURL()),
	addConsole(null, "Ready!");

	$('.container_login form').submit(function(event) {
		/* Act on the event */
		addConsole(event, "LoginAction");
		event.preventDefault();
		var $_post = $(this),
			username = $_post.find("input[name='username']").val(),
			password = $_post.find("input[name='password']").val();
		$.ajax({
			url: getURL() + 'core/functions/action.php?feed=x4sg7G8H9Rz',
			type: 'POST',
			dataType: 'json',
			data: {"username" : username, "password" : password},
		})
		.done(function(data) {
			if(data.error == "false") return;
			$('.alert_one').html("Error: " + data.str);
			$('.alert_one').css({'display':'block'});
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			addConsole("Error", errorThrown);
		})
		.always(function() {
		});
		
	});

	
	$('.container_register form').submit(function(event) {
		addConsole(event, "LoginAction");
		event.preventDefault();
		var $_post = $(this),
			username = $_post.find("input[name='reg_username']").val(),
			email = $_post.find("input[name='reg_mail']").val(),
			password = $_post.find("input[name='reg_pass']").val(),
			repass = $_post.find("input[name='reg_repass']").val(),
			birth = $_post.find("input[name='reg_birth']").val(),
			captcha = $_post.find("textarea[name='g-recaptcha-response']").val(),
			terms = $("input[name='reg_check']").prop('checked');
		$.ajax({
			url: getURL() + 'core/functions/action.php?feed=k8pG2Qg1r43ñ',
			type: 'POST',
			dataType: 'json',
			data: {"reg_username" : username, "reg_mail" : email, "reg_pass" : password, "reg_repass" : repass, "reg_birth" : birth, "reg_check" : terms, "g-recaptcha-response" : captcha},
		})
		.done(function(data) {
			console.log(data);
			if(data.error == "false") return;
			$('.alert_two').html("Error: " + data.str);
			$('.alert_two').css({'display':'block'});
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			addConsole("Error", errorThrown);
		})
		.always(function() {
		});
		
	});
});