<!DOCTYPE html>
<html>
<head>
	<title>%name% ~ {title}</title>
	<link rel="stylesheet" type="text/css" href="http://%host%/public/%theme%/styles/index.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="http://%host%/public/%theme%/js/global.js"></script>
</head>
<body>
	<b>%host%</b>
</body>
</html>