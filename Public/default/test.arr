<!DOCTYPE html>
<html>
<head>
	<title>%name% ~ {title}</title>
	<link rel="stylesheet" type="text/css" href="http://%host%/public/%theme%/styles/test.css">
</head>
<body>
	<header><a href="#"><div class="logo"></div></a><div class="online"><img src="http://%host%/public/%theme%/images/users.png"> 87 usuarios conectados</div></header>
	<div class="menu">
		<ul>
			<li class="tag blue"><a href="#">{tab1}</a></li>
			<li class="tag orange"><a href="#community">{tab2}</a></li>
			<li class="tag purple"><a href="#news">{tab3}</a></li>
			<li class="tag green"><a href="#team">{tab4}</a></li>
			<li class="tag yellow"><a href="#credits">{tab5}</a></li>
		</ul>
		<ul class="menu_two">
			<li class="tag green"><a href="#client">{tabClient}</a></li>
			<li class="tag purple"><a href="#asset">{tabAsset}</a></li>
			<li class="tag red"><a href="#logout">{tabLogout}</a></li>
		</ul>
	</div>
	<div class="container">		
	</div>
</body>
</html>