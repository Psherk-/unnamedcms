-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 12-12-2016 a las 12:57:28
-- Versión del servidor: 5.5.45
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unnamed_pages`
--

CREATE TABLE `unnamed_pages` (
  `url` varchar(25) DEFAULT NULL COMMENT '%host%/url',
  `page` varchar(25) DEFAULT NULL COMMENT '%host%/html',
  `login` enum('true','false','null') NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unnamed_pages`
--

INSERT INTO `unnamed_pages` (`url`, `page`, `login`) VALUES
('index', 'index', 'false'),
('maintenance', 'maintenance', 'null'),
('home', 'me', 'true');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unnamed_settings`
--

CREATE TABLE `unnamed_settings` (
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unnamed_settings`
--

INSERT INTO `unnamed_settings` (`value`) VALUES
('{\"host\":\"localhost/arrrq\",\"name\":\"ArrrqCMS\",\"captcha_public_key\":\"6LeuZg0UAAAAAJb-KfsrHG9c0Dq6NVTyqhbfkLbW\",\"captcha_private_key\":\"6LeuZg0UAAAAAPaRoWC7-1TqeI94Bj1jlqOBRjZc\",\"theme\":\"default\",\"language\":\"es_ES\",\"maintenance\":false,\"client_host\":\"127.0.0.1\",\"client_port\":\"30002\",\"external_text\":\"http://localhost/swf/gamedata/external_flash_texts/0695fc96678240c8da5700d4c018ff86e79c422a.txt\",\"external_vars\":\"http://localhost/swf/gamedata/external_variables.txt\",\"furnidata\":\"http://localhost/swf/gamedata/furnidata.xml\",\"figuredata\":\"http://localhost/swf/gamedata/figuredata.xml\",\"productdata\":\"http://localhost/swf/gamedata/productdata.txt\",\"gordon\":\"http://localhost/swf/gordon/PRODUCTION-201610182204-587747738/\",\"swf\":\"PRODUCTION-201610182204-587747738\"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `email` varchar(45) NOT NULL DEFAULT 'psherk@gmail.com',
  `username` varchar(45) NOT NULL DEFAULT 'Unnamed',
  `password` varchar(500) NOT NULL,
  `ticket` varchar(45) DEFAULT NULL,
  `sexo` enum('F','M') NOT NULL DEFAULT 'F',
  `mision` varchar(45) NOT NULL DEFAULT '# []',
  `look` varchar(95) NOT NULL DEFAULT 'hr-115-42.hd-190-1.ch-215-62.lg-285-91.sh-290-62	',
  `money` varchar(45) NOT NULL DEFAULT '9999;5000;10' COMMENT 'Credits;Duckets;Diamonds',
  `rank` int(1) NOT NULL DEFAULT '0',
  `room` int(1) NOT NULL DEFAULT '0',
  `birth` varchar(10) DEFAULT NULL,
  `connection` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `ticket`, `sexo`, `mision`, `look`, `money`, `rank`, `room`, `birth`, `connection`) VALUES
(1, 'psherk@gmail.com', 'Unnamed', '$2a$08$ulr5UwUBxHOnwGtdmprEAe6H03A8vQU7bHHpWOnTWHOg1/upsOvgm', NULL, 'M', '# []', 'hr-115-42.hd-190-1.ch-215-62.lg-285-91.sh-290-62	', '9999;5000;10', 0, 0, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
