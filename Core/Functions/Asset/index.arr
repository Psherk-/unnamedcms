<!DOCTYPE html>
<html>
<head>
	<title>%name% ~ {title}</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="logo">Panel de administración</div>
		<div class="">
			<form method="post">
				<input type="text" name="username" placeholder="Username">
				<input type="password" name="password" placeholder="Password">
				<input type="password" name="pin" placeholder="PIN">
				<input type="submit" value="Login">
			</form>
		</div>
		<div class="error">Pin incorrecto</div>
	</div>
</body>
</html>