<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
 *
 * http://localhost/arrrq/Core/Funtions/global_function.php?feed= ?
*/
require '../MainCore.php';

header('Content-type: application/json; charset=utf-8');
if(isset($_GET["feed"]))
{
	if($_GET["feed"] == "x4sg7G8H9Rz")
	{
		$UserController->Authenticate([$_POST["username"], $_POST["password"]]);
	}
	else if($_GET["feed"] == "k8pG2Qg1r43ñ")
	{
		$UserController->Registration([$_POST["reg_username"], $_POST["reg_mail"], $_POST["reg_pass"], $_POST["reg_repass"], $_POST["reg_birth"], $_POST["reg_check"], $_POST["g-recaptcha-response"]]);
	}
	else
	{
		echo json_encode($_SERVER['REMOTE_ADDR']);
	}
} else {
	echo json_encode($_SERVER['REMOTE_ADDR']);
}
exit();
?>