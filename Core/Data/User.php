<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
class User
{
  /*
  * Params @Money[num] = 0 Creditos, 1 Duckets, 2 Diamonds
  */
	 private $Id, $Username, $Password, $Email, $Ticket, $Money, $Gender, $Mision, $Look, $Rank, $Birth, $Connection;

  	public function __construct($id, $username, $password, $email, $ticket, $money, $gender, $mision, $look, $rank, $birth, $connection) {
    	$this->Id = $id;
    	$this->Username = $username;
    	$this->Password = $password;
    	$this->Email = $email;
    	$this->Ticket = $ticket;
    	$this->Gender = $gender;
    	$this->Money = explode(';', $money);
    	$this->Mision = $mision;
    	$this->Look = $look;
    	$this->Rank = $rank;
      $this->Birth = $birth;
    	$this->Connection = $connection;
  	}

  	/* Getters */
  	public function getId() {
    	return $this->Id;
  	}
  	public function getUsername() {
    	return $this->Username;
  	}
  	public function getPassword() {
    	return $this->Password;
  	}
  	public function getEmail() {
    	return $this->Email;
  	}
  	public function getTicket() {
      $this->setTicket();
    	return $this->Ticket;
  	}
  	public function getGender() {
  		return $this->Gender;
  	}
  	public function getMoney($v) {
    	if(!is_numeric($v) || empty($this->Money[$v]))
      		return 0;
    	return $this->Money[$v];
  	}
  	public function getMision() {
  		return $this->Mision;
  	}
  	public function getLook() {
  		return $this->Look;
  	}
  	public function gerRank() {
  		return $this->Rank;
  	}
    public function gerBirth() {
      return $this->Birth;
    }
  	public function getConnection() {
  		return date('d-m-Y H:i:s', $this->Connection);
  	}

  	/* Setters */
  	public function setEmail($x) {
  		$this->Email = $x;
  	}
  	public function setPassword($x) {
  		$this->Password = $x;
  	}
    public function setTicket() {
    }
  	public function setConnection($x) {
  		$this->Connection = $x;
  	}
}
?>