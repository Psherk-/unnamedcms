<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
class Page
{
	public function __construct() { $this->parseLang(); }
	public $Lang = [];
	private function parseLang()
	{
		GLOBAL $IEnvironment;
		$file = "./Core/Languages/" . $IEnvironment->getConfig["server"]["language"] . ".ini";
		if(file_exists($file))
		{
			$gestor = fopen($file, 'r');
			$contec = fread($gestor, filesize($file));
			fclose($gestor);
			foreach ($IEnvironment->getConfig["server"] as $key => $value)
				$contec = str_replace('%'.$key.'%', $value, $contec);
			$this->Lang = parse_ini_string($contec, true);
		}
	}
	public function ConstructPages()
	{
		GLOBAL $IEnvironment, $MySqlConnection, $User;
		$URL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		if(strlen($URL) <= strlen($IEnvironment->getConfig["server"]["host"]) + 1)
			$URL = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."index";
		$URL = substr($URL, strlen($IEnvironment->getConfig["server"]["host"]) + 1);
		$URL = str_replace([".html", ".php"], "", $URL);
		$parseURL = explode("/", $URL);
		$MySqlConnection->Statement("SELECT * FROM unnamed_pages WHERE url = ?", "dataRow");
		if(count($parseURL) == 1)
		{
			$MySqlConnection->addParameter($parseURL[0]);
			if(count($MySqlConnection->getData()) == 1)
			{
				if($IEnvironment->getConfig["server"]["maintenance"] && $parseURL[0] != "maintenance")
				{
					header("location: http://".$IEnvironment->getConfig["server"]["host"]."/maintenance");
					return;
				}
				else if(!$IEnvironment->getConfig["server"]["maintenance"] && $parseURL[0] == "maintenance")
				{
					header("location: http://".$IEnvironment->getConfig["server"]["host"]."/");
					return;
				}
				foreach ($MySqlConnection->getData() as $dRow) {
					if($dRow["login"] == "true")
					{
						if($User == null)
							header("location: http://".$IEnvironment->getConfig["server"]["host"]."/");
					}
					elseif($dRow["login"] == "false")
					{
						if($User != null)
							header("location: http://".$IEnvironment->getConfig["server"]["host"]."/home");
					}
					$this->View($dRow["page"]);
				}
			}
			else
				header("location: http://".$IEnvironment->getConfig["server"]["host"]."/");
		}
		else
		{
			if($parseURL[0] == "function")
			{
				$this->JqueryAction($parseURL[1]);
				return;
			}
			else if($parseURL[0] == "asset")
			{
				$MySqlConnection->addParameter($parseURL[1]);
				$this->View($parseURL[1], true);
			}
			else
			{
				header("location: http://".$IEnvironment->getConfig["server"]["host"]."/");
			}
		}
	}
	private function JqueryAction($arg)
	{
		GLOBAL $UserController;

		header('Content-type: application/json; charset=utf-8');
		if($arg == "x4sg7G8H9Rz")
			$UserController->Authenticate([$_POST["username"], $_POST["password"]]);
		else if($arg == "k8pG2Qg1r43ñ")
			$UserController->Registration([$_POST["reg_username"], $_POST["reg_mail"], $_POST["reg_pass"], $_POST["reg_repass"], $_POST["reg_birth"], $_POST["reg_check"], $_POST["g-recaptcha-response"]]);
		else
			echo json_encode($_SERVER['REMOTE_ADDR']);
		exit();
	}
	private function View($arg, $manage = false)
	{
		GLOBAL $IEnvironment, $UserController;

		if($manage)
			$folder = __CORE__ . '/Functions/asset/';
		else
			$folder = './Public/'.$IEnvironment->getConfig["server"]["theme"].'/';
		if(!file_exists($folder . $arg . '.arr'))
		{
			$arg = "404";
			$file = $folder . '404.arr';
		}
		else
			$file = $folder . $arg . ".arr";//$file = $folder . "test.arr";$arg="test";
		$gestor = fopen($file, 'r');
		$contec = fread($gestor, filesize($file));
		fclose($gestor);

		/* Server parameters */
		foreach ($IEnvironment->getConfig["server"] as $key => $value)
			$contec = str_replace('%'.$key.'%', $value, $contec);


		/*Language parameters*/
		if(!in_array($arg, $this->Lang))
		{
			foreach ($this->Lang[$arg] as $key => $value)
				$contec = str_replace('{'.$key.'}', $value, $contec);
		}

		$contec = $UserController->parseVars($contec);

		echo $contec;
	}
}
?>