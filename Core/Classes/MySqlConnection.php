<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
class MySqlConnection
{
	protected $_connection;
	private $_sql, $_type, $_send;
	private $_parameters = [];
	public function __construct()
	{
		GLOBAL $IEnvironment;
		if($this->_connection != null)
			return;

		try
		{
			$this->_connection = new PDO("mysql:host=".$IEnvironment->getConfig["MySQL"]["host"].";port=".$IEnvironment->getConfig["MySQL"]["port"].";dbname=".$IEnvironment->getConfig["MySQL"]["database"]."", $IEnvironment->getConfig["MySQL"]["username"], $IEnvironment->getConfig["MySQL"]["password"]);
			$this->serverData();
		}
		catch(PDOException $e)
		{
			echo '<b>[Error] =></b> ' . $e;
		}
	}
	public function Statement($sql, $type)
	{
		if(empty($sql) || empty($type))
			return;

		$this->_parameters = [];
		$this->_sql = $sql;
		$this->_type = $type;
		$this->_send = null;
	}
	public function addParameter($obj)
	{
		if(is_array($obj))
		{
			foreach ($obj as $value)
				array_push($this->_parameters, $value);	
		}
		else
			array_push($this->_parameters, $obj);
	}
	private function setType()
	{
		switch ($this->_type) {
			case 'dataTable':
				$sql = $this->_connection->prepare($this->_sql);
				$sql->execute($this->_parameters);
				$this->_send = $sql->fetchAll();
				break;
			case 'dataRow':
				$sql = $this->_connection->prepare($this->_sql);
				$sql->execute($this->_parameters);
				$this->_send = $sql->fetchAll();
				break;
			case 'String':
				$sql = $this->_connection->prepare($this->_sql);
				$sql->execute($this->_parameters);
				break;
		}
	}
	public function getData()
	{
		if($this->_sql != null)
			$this->setType();
		return $this->_send;
	}
	public function Destroy()
	{
		if($this->_connection != null)
			$this->_connection = null;
	}

	/*Server Params*/
	private function serverData()
	{
		global $IEnvironment;
		$this->Statement("SELECT * FROM unnamed_settings WHERE 1", "dataRow");
		foreach($this->getData() as $dRow)
		{
			$str = json_decode($dRow["value"]);
			foreach($str as $key => $val)
				$IEnvironment->getConfig["server"][$key] = $val;				
		}
	}
}
?>