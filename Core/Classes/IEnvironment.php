<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
class IEnvironment
{
	public $getConfig = [];
	public function __construct()
	{
		$this->setConfiguration();
		$this->setLanguage();
	}
	private function setConfiguration()
	{
		if(file_exists(__CORE__ . '/configData.json'))
		{
			$json = json_decode(file_get_contents(__CORE__ . '/configData.json'), true);
			foreach ($json as $key => $value)
				$this->getConfig[$key] = $value;
		} else {
			echo "<b>[Error] =></b> Configuration file does not exist!";
		}
	}
	private function setLanguage()
	{
		$this->getConfig["languages"] = [];
		if(is_dir(__CORE__ . '/Languages') && $page = opendir(__CORE__ . '/Languages'))
		{
			while ($file = readdir($page)) {
				if(substr($file, -4) == ".ini")
					array_push($this->getConfig["languages"], substr($file, 0, -4));
			}
		}
	}
}
?>