<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
ini_set("display_errors", true);
define("__CORE__", __DIR__);

function InitServer() {
	$Array = ["Classes", "Controllers", "Data"];
	foreach ($Array as $dRow) {
		# code...
		if(is_dir(__CORE__ . '/' . $dRow  . '/')) {
			if($folder = opendir(__CORE__ . '/' . $dRow  . '/')) {
				while(($file = readdir($folder)) !== false) {
					if($file != '.' && $file != '..' && $file != '.htaccess') {
						require __CORE__ . '/' . $dRow . '/' . $file;
						$file = substr($file, 0, -4);
						if($dRow != "Data") $GLOBALS[$file] = new $file();
					}
				}
			}
		}
	}
}
InitServer();
?>