<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
class UserController extends Controller
{
	public $UsersConnect = [];
	public function __construct() { @session_start(); }

	public function parseVars($arg)
	{
		GLOBAL $User;
		if($User != null)
		{
			$session = [
				"id" => $User->getId(),
				"username" => $User->getUsername(),
				"password" => $User->getPassword(),
				"mail" => $User->getEmail(),
				"ticket" => $User->getTicket(),
				"gender" => $User->getGender(),
				"credits" => $User->getMoney(0),
				"duckets" => $User->getMoney(1),
				"diamonds" => $User->getMoney(2),
				"mision" => $User->getMision(),
				"look" => $User->getLook(),
				"rank" => $User->gerRank(),
				"birth" => $User->gerBirth(),
				"connection" => $User->getConnection()
			];
			foreach ($session as $key => $value) {
				$arg = str_replace('%'.$key.'%', $value, $arg);
			}
		}
		return $arg;
	}

	public function Authenticate($arg)
	{
		GLOBAL $IEnvironment, $MySqlConnection, $PasswordHash, $User;

		if($arg[0] != null && $arg[1] != null && $User == null)
		{
			$arg[0] = $this->protectedStr($arg[0]);
			$MySqlConnection->Statement("SELECT * FROM users WHERE (username = ? OR email = ?)", "dataRow");
			$MySqlConnection->addParameter([$arg[0], $arg[0]]);
			if(count($MySqlConnection->getData()) > 0)
			{
				foreach($MySqlConnection->getData() as $dRow)
				{
					if($PasswordHash->CheckPassword($arg[1], $dRow["password"]))
					{
						$GLOBALS['User'] = new User($dRow['id'], $dRow['username'], $dRow['password'], $dRow['email'], $dRow['ticket'], $dRow['money'], $dRow['sexo'], $dRow['mision'], $dRow['look'], $dRow['rank'], $dRow['birth'], $dRow['connection']);
						$_SESSION["user"] = $dRow['id'];
						$this->ReturnJson(["error"=>"false", "redirect"=>"me"]);
						return;
					}
					else
						$alert = ["error"=>"true", "str" => "{login.error.four}"];
				}
			}
			else
				$alert = ["error"=>"true","str"=>"{login.error.five}"];
		}
		else
		{
			if($arg[0] == null && $arg[1] != null)
				$alert = ["error"=>"true","str"=>"{login.error.two}"];
			else if($arg[1] == null && $arg[0] != null)
				$alert = ["error"=>"true","str"=>"{login.error.three}"];
			else
				$alert = ["error"=>"true","str"=>"{login.error.one}"];
		}
		$this->ReturnJson($alert);
	}
	public function Registration($arg)
	{
		GLOBAL $MySqlConnection, $PasswordHash, $reCaptcha;

		if($arg[0] != null && $arg[1] != null && $arg[2] != null && $arg[3] != null && $arg[4] != null && $arg[6] != null)
		{
			foreach ($arg  as $key => $value)              //SQL Inyection
				$arg[$key] = $this->protectedStr($value); //SQL Inyection

			if(strlen($arg[0]) >= 4)
			{
				$MySqlConnection->Statement("SELECT * FROM users WHERE username = ?", "dataRow");
				$MySqlConnection->addParameter($arg[0]);
				if(!count($MySqlConnection->getData()) > 0)
				{
					if(strstr($arg[1], '@') && strstr($arg[1], ['.com', '.org', '.net', '.es']))
					{
						$MySqlConnection->Statement("SELECT * FROM users WHERE email = ?", "dataRow");
						$MySqlConnection->addParameter($arg[1]);
						if(!count($MySqlConnection->getData()) > 0)
						{
							if(strlen($arg[2]) >= 7)
							{
								if($arg[2] == $arg[3])
								{
									if(true)
									{
										$response = $reCaptcha->verify($arg[6]);
										if(!$response->isSuccess())
										{
											if($arg[5] == "true")
											{
												$MySqlConnection->Statement("INSERT INTO users (email, username, password, birth, connection) VALUES (?, ?, ?, ?, ?)", "String");
												$MySqlConnection->addParameter($arg[1]);
												$MySqlConnection->addParameter($arg[0]);
												$MySqlConnection->addParameter($PasswordHash->HashPassword($arg[2]));
												$MySqlConnection->addParameter($arg[4]);
												$MySqlConnection->addParameter(time());
												$MySqlConnection->getData();
												$this->Authenticate([$arg[0], $arg[2]]);
												return;
											}
											else
												$alert = ["error"=>"true", "str"=>"Acepta los términos del contrato"];
										}
										else
											$alert = ["error"=>"true", "str"=>"Catpcha malo"];
									}
									else
										$alert = ["error"=>"true", "str"=>"Fecha de nacimiento erronea"];
								}
								else
									$alert = ["error"=>"true", "str"=>"Las contraseñas no coinciden"];
							}
							else
								$alert = ["error"=>"true", "str"=>"La contraseña es demasiado corta, minimo 7 caracteres"];
						}
						else
							$alert = ["error"=>"true", "str"=>"El email ya esta en uso"];
					}
					else
						$alert = ["error"=>"true", "str"=>"Email invalido"];
				}
				else
					$alert = ["error"=>"true", "str"=>"El nombre de usuario ya esta en uso"];
			}
			else
				$alert = ["error"=>"true", "str"=>"La nombre de usuario es demasiado corto, minimo 4 caracteres"];
		}
		else
		{
			$alert = ["error"=>"true","str"=>"Completa todos los campos!"];
		}
		$this->ReturnJson($alert);
	}
	public function UpdateUser($arg)
	{
		GLOBAL $User;
	}
}
?>