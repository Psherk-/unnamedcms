<?php
/*
 * @ProjectName UnnamedCMS
 * @Description Content Management System developed in php
 * @Version 1.0.0
 * @Author Psherk
*/
use ReCaptcha\ReCaptcha;

class Controller
{
	public function __construct() { $this->ReCaptcha(); }

	private function ReCaptcha()
	{
		GLOBAL $IEnvironment;

		require __CORE__ . '/Functions/Captcha.php';
		$GLOBALS["reCaptcha"] = new ReCaptcha($IEnvironment->getConfig["server"]["captcha_private_key"]);
	}

	public function ReturnJson($arg, $out = true)
	{
		$respose = $this->parseArray($arg);
		if($out == true)
			echo json_encode($respose);
		else
			return json_encode($respose);
	}

	private function parseArray($arg)
	{
		GLOBAL $Page;

		/*if(in_array("errors", $Page->Lang))
		{
			foreach ($arg as $key => $value) {
				foreach ($Page->Lang["errors"] as $Pkey => $Pvalue) {
					if(strstr($value, '{' . $Pkey . '}'))
						$arg[$key] = str_ireplace('{' . $Pkey . '}', $Pvalue, $value);
				}
			}
		}*/
		return $arg;
	}

	public function protectedStr($arg)
	{
		return $arg;
	}
}
?>